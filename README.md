# gallery-management

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Explanation design and assumptions

```
In this source I use: 
- VueJs framework
- Vuetify for view
- Vuex for management state
- Faker for render fake data
- Js for random number pass into function render number of gallery

In src/api/index.js

- This file use for call Api from Backend.
- In my case I use it for render fake Data and handle favourite album, image or render id and sort list album, album detail

In store folder

- I change store.js default of VueJs to index.js so I must change import of main.js
- Create folder modules for spilit code for different module that make me easy to manage

This source will have some problem because I make fake data not from BE so when click back or next on browser I can send id for server then get album, I must random when back or next on browser.
And about title of album equal height(Css can do that but not support for all browser: webkit-line-clamp, ...).
- Backend must return same number of character then I'll set height for it.
- Or I can write JS code slice character from BE return. But in this source I still not do this.

If have any infomation or dont know something please contact me via mail nguyenf5n@gmail.com

Thanks !
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
