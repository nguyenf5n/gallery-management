export const ApiService = {
  renderFakeData(length, type) {
    let fakeData = [],
        faker = require('faker')

    for (let i = 0; i < length; i++) {
      let temp = {
        id: i,
        name: faker.name.firstName(),
        description: `${faker.lorem.lines().slice(0, 1000)} ...`,
        image: faker.image.avatar(),
        isFavorite: faker.random.boolean(),
        type: type
      }

      if(type !== 'albumDetail') {
        temp.numberOfImages = this.getRandomArbitrary(5, 20)
        temp.numberOfVideo = this.getRandomArbitrary(5, 20)
      }

      fakeData.push(temp)
    }

    return fakeData
  },
  handleFavoriteAlbumServer(data, id) {
    let results = data
    results.filter((result) => {
      result.id === id && (result.isFavorite = !result.isFavorite)
    })

    return results
  },
  updateImageDetail(albumDetail, imageInfo) {
    albumDetail.filter((result) => {
      if(result.id === imageInfo.id) {
        result.name = imageInfo.name
        result.description = imageInfo.description
      }
    })

    return albumDetail
  },
  handleUpdateAlbumDetailServer(data) {
    let newData = data.newData,
        fakeData = data.fakeData,
        length = data.length,
        type = data.type,
        results = {},
        faker = require('faker')

    newData.map((item) => {
      let temp = {
        id: parseInt(item.id + length),
        name: item.name,
        description: `${faker.lorem.lines().slice(0, 1000)} ...`,
        image: item.src,
        isFavorite: false,
        type: type
      }

      fakeData.push(temp)
    })

    fakeData.sort((a, b) => (a.id > b.id) ? 1 : -1)
    results = {
      fakeData: fakeData.reverse(),
      type: type
    }

    return results
  },
  getRandomArbitrary(min, max) {
    return parseInt(Math.random() * (max - min) + min);
  }
};

export default ApiService
