import Vue from 'vue'
import Vuex from 'vuex'
import albumlist from './modules/albumlist'
import albumdetail from './modules/albumdetail'
import images from './modules/images'

Vue.use(Vuex)

const state = {

}

const getters = {

}

const mutations = {

}

const actions = {

}

const modules = {
  albumlist,
  albumdetail,
  images
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules
})
