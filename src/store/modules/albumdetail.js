import ApiService from '@/api'

export default {
  namespaced: true,
  state: {
    albumDetail: [],
    currAlbumDetail: {},
    dialogLightBox: false,
    currImageLightBox: {}
  },
  // return state
  getters: {
    getAlbumDetail(state) {
      return state.albumDetail
    },
    getDialogLightBoxStatus(state) {
      return state.dialogLightBox
    },
    getCurrImageLightBox(state) {
      return state.currImageLightBox
    },
    getCurrAlbumDetail(state) {
      return state.currAlbumDetail
    }
  },
  // handle state and data
  mutations: {
    setAlbumDetail(state, data) {
      state.albumDetail = data
    },
    updateAlbumDetail(state, data) {
      state.albumDetail = data
    },
    setCurrAlbumDetail(state, data) {
      state.currAlbumDetail = data
    },
    setDialogLightBoxStatus(state) {
      state.dialogLightBox = !state.dialogLightBox
    },
    setCurrImageLightBox(state, data) {
      state.currImageLightBox = data
    }
  },
  // commit mutations with data
  actions: {
    getData(context, length) {
      // call ApiServer or render Fake data
      return new Promise(resolve => {
        let fakeAlbumDetail = ApiService.renderFakeData(length, 'albumDetail')
        context.commit('setAlbumDetail', fakeAlbumDetail.reverse())
        resolve(fakeAlbumDetail)
      })
    },
    handleFavoriteImage(context, itemInfo) {
      // call ApiServer
      return new Promise(resolve => {
        let fakeData = context.state.albumDetail
        fakeData = ApiService.handleFavoriteAlbumServer(fakeData, itemInfo.id)
        context.commit('setAlbumDetail', fakeData)
        resolve(fakeData)
      })
    },
    handleUpdateAlbumDetail(context, data) {
      return new Promise(resolve => {
        context.commit('updateAlbumDetail', data.fakeData)
        resolve(data)
      })
    },
    handleUpdateCurrAlbumDetail(context, data) {
      return new Promise(resolve => {
        context.commit('setCurrAlbumDetail', data)
        resolve(data)
      })
    },
    handleShowHideDialog(context) {
      return new Promise(resolve => {
        context.commit('setDialogLightBoxStatus')
        resolve()
      })
    },
    handleSetCurrImageLightBox(context, data) {
      return new Promise(resolve => {
        context.commit('setCurrImageLightBox', data)
        resolve(data)
      })
    },
    handleUpdateLightBox(context, id) {
      return new Promise(resolve => {
        let result,
            temp

        temp = context.state.albumDetail.filter((image) => {
          return image.id === id
        })

        result = {
          id: temp[0].id,
          name: temp[0].name,
          description: temp[0].description,
          image: temp[0].image,
          isFavorite: temp[0].isFavorite,
          type: temp[0].type
        }

        context.commit('setCurrImageLightBox', result)
        resolve(result)
      })
    },
    updateLengthAlbumDetailAfterCreate(context) {
      return context.state.albumDetail.length
    },
    updateImageDetail(context, data) {
      return new Promise(resolve => {
        let albumDetail = ApiService.updateImageDetail(context.state.albumDetail, data)
        context.commit('updateAlbumDetail', albumDetail)
        resolve(data)
      })
    }
  }
}
