import ApiService from '@/api'

export default {
  namespaced: true,
  state: {
    albumList: {
      album : {
        title: 'Album',
        data: []
      },
      media : {
        title: 'Media',
        data: []
      }
    },
    lengthAlbumDetail: 0
  },
  // return state
  getters: {
    getAlbumAndMedia(state) {
      return state.albumList
    },
    getLengthAlbumDetail(state) {
      return state.lengthAlbumDetail
    }
  },
  // handle state and data
  mutations: {
    setAlbumList(state, data) {
      state.albumList.album.data = data
    },
    setMediaList(state, data) {
      state.albumList.media.data = data
    },
    setFakeData(state, data) {
      state.albumList[data.type].data = data.fakeData
    },
    updateAlbum(state, data) {
      state.albumList[data.type].data.unshift(data.album)
    },
    updateLengthAlbumDetail(state, data) {
      state.lengthAlbumDetail = data
    },
  },
  // commit mutations with data
  actions: {
    getData(context, length) {
      // call ApiServer or render Fake data
      return new Promise(resolve => {
        let fakeAlbum = ApiService.renderFakeData(length, 'album'),
            fakeMedia = ApiService.renderFakeData(length, 'media')
        context.commit('setAlbumList', fakeAlbum)
        context.commit('setMediaList', fakeMedia)
        resolve(fakeAlbum, fakeMedia)
      })
    },
    handleFavoriteAlbum(context, itemInfo) {
      // call ApiServer or fake Api myself
      return new Promise(resolve => {
        let fakeData = context.state.albumList[itemInfo.type].data
        fakeData = ApiService.handleFavoriteAlbumServer(fakeData, itemInfo.id)
        context.commit('setFakeData', {fakeData: fakeData, type: itemInfo.type})
        resolve(fakeData)
      })
    },
    handleUpdateAlbum(context, data) {
      return new Promise(resolve => {
        context.commit('updateAlbum', data)
        resolve(data)
      })
    },
    handleUpdateLengthAlbumDetail(context, data) {
      return new Promise(resolve => {
        context.commit('updateLengthAlbumDetail', data)
        resolve(data)
      })
    }
  }
}
