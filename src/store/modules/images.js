import ApiService from '@/api'

export default {
  namespaced: true,
  state: {
    album: {},
    isValid: true,
    images: []
  },
  // return state
  getters: {},
  // handle state and data
  mutations: {
    setImageAlbum(state, data) {
      state.album.image = data
    },
    setError(state, data) {
      state.isValid = data
    },
    setAlbum(state, data) {
      state.album = data
    },
    setImagesDetail(state, data) {
      state.images = data
    },
    clearAlbum(state, data) {
      state.album = data
    },
    clearImages(state, data) {
      state.images = data
    },
    clearAlbumDetail(state) {
      state.images = []
    }
  },
  // commit mutations with data
  actions: {
    getImageAlbum(context, image) {
      context.commit('setImageAlbum', image)
    },
    getImagesDetail(context, images) {
      context.commit('setImagesDetail', images)
    },
    handleCreateAlbum(context, album) {
      context.commit('setAlbum', album)
      return album
    },
    handleCreateAlbumDetail(context, data) {
      return new Promise(resolve => {
        // update id for image fake server
        let results,
            obj = {
              fakeData: data.albumDetail,
              length: data.albumDetail.length,
              newData: data.newData.images,
              type: data.newData.type
            }

        results = ApiService.handleUpdateAlbumDetailServer(obj)
        resolve(results)
      })
    },
    clearAlbumState(context) {
      let album = {}

      context.commit('clearAlbum', album)
    },
    clearAlbumDetailState(context) {
      context.commit('clearAlbumDetail')
    }
  }
}
